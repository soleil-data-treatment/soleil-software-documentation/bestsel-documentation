# Aide et ressources de BeStSel pour Synchrotron SOLEIL

[<img src="http://bestsel.elte.hu/Bestsel_elemei/BeStSel_logo.png" width="500"/>](http://bestsel.elte.hu)

## Résumé

- Reconnaissance de repliements de protéines à partir de spectres de CD // SRCD
- Collab. SOLEIL, Univ. Budapest

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Navigation rapide

| Fichiers téléchargeables |
| - |
| [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/bestsel-documentation/-/tree/master/documents) |

## Installation

- Systèmes d'exploitation supportés: Linux, Machine virtuelle
- Installation: Web utilisation sur un serveur

## Format de données

- en entrée: ASCII,
- en sortie: Rapport d'analyse en pdf png, TXT
- serveur: http://bestsel.elte.hu/
